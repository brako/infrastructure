#!/bin/bash -x

printf "\n[pipeline-start] $(date)\n\n"

export REQUEST_ID=$1
RUN_DIR=$2
REQUEST_TIMEOUT=$3
TASK_NAME="tmt"

ARTIFACTS_HOSTNAME=artifacts.dev.testing-farm.io
ARTIFACTS_ROOT=/archive

ARTIFACTS_URL="https://artifacts.dev.testing-farm.io"
ISSUES_URL="https://gitlab.com/testing-farm/general/-/issues"

PROGRESS_TICK=30

[ -z "$REQUEST_TIMEOUT" ] && REQUEST_TIMEOUT=12h

[ -z "$REQUEST_ID" ] && { echo "No request ID given"; exit 1; }
[ -z "$RUN_DIR" ] && { echo "No run directory given"; exit 1; }

REQUEST_ARCHIVE_DIR=${ARCHIVE_DIR}/${REQUEST_ID}

archive() {
    WHAT="$1"
    WHERE="$2"
    OPTIONS="$3"

    if [ ! -e $WHAT ]; then
        echo "Artifact '$WHAT' not found, skipping archivation"
        return
    fi

    rsync --links --perms --times --verbose --compress $OPTIONS "$WHAT" ${ARTIFACTS_HOSTNAME}:${ARTIFACTS_ROOT}/${REQUEST_ID}/${WHERE}
}

create_results_xml() {
    STATE=$1
    RESULT=$2
    RESULTS_XML=${3:-results.xml}

    cat > $RESULTS_XML <<EOF
<testsuites overall-result="$RESULT">
 <testsuite name="pipeline">
  <testcase name="$STATE" result="$RESULT">
   <logs>
     <log name="pipeline.log" href="${ARTIFACTS_URL}/${REQUEST_ID}/pipeline.log"/>
   </logs>
  </testcase>
 </testsuite>
</testsuites>
EOF
}

create_results() {
    echo "started creating results artifacts"

    RESULTS_XML=${1:-results.xml}

    # citool exposes all output to stderr, archive it as the pipeline log
    # TFT-894 nomad splits the stderr and stdout into multiple files in case of big logs
    cat ${RUN_DIR}/logs/${TASK_NAME}.stderr.[0-9]* > pipeline.log
    archive pipeline.log

    tfxunit2junit.py --issues-url $ISSUES_URL $RESULTS_XML > results-junit.xml || true
    archive results-junit.xml

    echo "finished creating results artifacts"
}

# create archive directory
if [ -z "$NO_ARCHIVE" ]; then
    ssh $ARTIFACTS_HOSTNAME mkdir -p ${ARTIFACTS_ROOT}/${REQUEST_ID}
fi

# rpmlist on worker
rpm -qa > rpmlist-worker.txt

# create in progress page
create_results_xml in-progress skipped progress.xml

# deploy oculus viewer
curl -s --fail --retry 5 --show-error -o index.html https://gitlab.com/testing-farm/oculus/-/raw/main/results.html
archive index.html

# disable bash debug until we run tmt with the progress reporting
set +x

# start syncing in background
if [ -z "$NO_ARCHIVE" ]; then
    { while true; do create_results progress.xml &>> progress.log; sleep $PROGRESS_TICK; done; } &
fi

# get citool container image from request
CITOOL_IMAGE=$(curl -s --retry 5 https://api.dev.testing-farm.io/v0.1/requests/$REQUEST_ID | jq -r '.settings.worker.image | select (.!=null)')
[ -n "$CITOOL_IMAGE" ] && export CITOOL_IMAGE

# run citool via container with a timeout
timeout --preserve-status --foreground --signal=SIGUSR1 $REQUEST_TIMEOUT \
citool-container.sh hide-secrets \
                    rules-engine \
                    testing-farm-request --request-id $REQUEST_ID \
                    ansible \
                    artemis \
                    fedora-copr:copr \
                    guest-setup-testing-farm:guest-setup \
                    install-copr-build \
                    install-koji-build-execute \
                    install-repository \
                    guess-environment-testing-farm-request:guess-environment \
                    dist-git-testing-farm:dist-git \
                    coldstore \
                    testing-farm-request-state-reporter \
                    test-schedule-tmt-connect:test-schedule-tmt \
                    test-scheduler \
                    test-schedule-runner \
                    test-schedule-report

# skip archivation
[ -n "$NO_ARCHIVE" ] && exit

# kill the progress and wait for the process to finish
echo "stopping process progress generation (PID $!)"
kill $!
wait $!

# progress is over, enable back bash debug mode
set -x

# create pipeline error if no results.xml available
if [ ! -e "results.xml" ]; then
    create_results_xml failure error
fi

# sync task logs; we need to be picky here to avoid copying multipe copies of the built/unbuilt source tree
# until tmt does that by itself: https://github.com/teemtee/tmt/issues/1707
TASK_DIR=$(dirname $RUN_DIR)
for path in ${TASK_DIR}/${TASK_NAME}/*; do
    fname="$(basename "$path")"
    if [ -f "$path" ]; then
        # all files in the top-level directory, like ansible-output.txt or progress.xml
        archive $path
    elif [ "${fname#guest-setup}" != "$fname" ]; then
        # all guest setup logs
        archive $path '' '--recursive'
    elif [ "${fname#work-}" != "$fname" ]; then
        # all TMT plan results
        archive $path '' '--recursive --exclude tree'
    fi
done

create_results

printf "\n[pipeline-end] $(date)"

# save stdout and stderr at the very end
cat ${RUN_DIR}/logs/${TASK_NAME}.stdout.[0-9]* > job.log
archive job.log
cat ${RUN_DIR}/logs/${TASK_NAME}.stderr.[0-9]* > pipeline.log
archive pipeline.log
