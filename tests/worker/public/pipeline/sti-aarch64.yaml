---

marks:
  - public

variables:
  ARCH: aarch64
  COMPOSE: Fedora-Rawhide
  REQUEST_ID: 815f9001-af4a-46c2-9fe9-cddd891fe31b

pipeline: |
  hide-secrets
  rules-engine
  testing-farm-request --request-id {{ REQUEST_ID }}
  ansible
  artemis
  fedora-copr:copr
  guest-setup-testing-farm:guest-setup
  install-copr-build
  install-koji-build-execute
  install-repository
  git
  coldstore
  testing-farm-request-state-reporter
  test-scheduler-sti
  test-schedule-runner-sti
  test-scheduler-testing-farm
  test-schedule-runner
  test-schedule-report

checks:
  literal-strings:
    - "[testing-farm-request] Connected to Testing Farm Service 'https://internal.api.dev.testing-farm.io'"
    - "[testing-farm-request] Initialized with {{ REQUEST_ID }}"
    - "[artemis] Using Artemis API"
    - "[git] <RemoteGitRepository(clone_url=https://gitlab.com/testing-farm/tests, branch=not specified, ref=main)>"
    - "[testing-farm-request-state-reporter] pipeline complete"
    - "[test-scheduler-sti] cloning repo"
    - "[test-schedule-runner] running test schedule of 1 entries:"
    - "[test-schedule-runner] 1 entries pending:"
    - "[test-schedule-runner] 0 entries pending:"
    - "[test-schedule-report] Result of testing: PASSED"

  literal-patterns:
    - "[coldstore] For the pipeline artifacts, see https://artifacts.dev.testing-farm.io/{{ REQUEST_ID }}"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest is being provisioned"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest is ready"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest has become alive"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest provisioned"
    - "[artemis] [{{ artemis_guestname_pattern }}] destroying guest"
    - "[artemis] [{{ artemis_guestname_pattern }}] successfully released"

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - CREATED
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - ""
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - GUEST_PROVISIONING
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - ""
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - GUEST_SETUP
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - RUNNING
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - CLEANUP
          - OK
          - PASSED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - COMPLETE
          - OK
          - PASSED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

  artifacts:
    - "guest-setup-{{ artemis_guestname_pattern }}"
    - "guest-setup-{{ artemis_guestname_pattern }}/guest-setup-output-pre-artifact-installation.txt"
    - "guest-setup-{{ artemis_guestname_pattern }}/guest-setup-output-post-artifact-installation.txt"
    - "citool-debug\\.txt"
    - "citool-debug\\.verbose\\.txt"
    - "results\\.xml"
